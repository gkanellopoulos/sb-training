import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Input from './Input.js';
import Output from './Output.js';

class App extends Component {
  constructor(props) {
   super(props);
   this.state = {
      inputMessage: 'No text...',
      checkboxValue: false
   };

   this.handleInputChange = this.handleInputChange.bind(this);
   this.handleCheckboxChange = this.handleCheckboxChange.bind(this);

  }

  handleInputChange(event) {
        this.setState({
            inputMessage: event.target.value,
        });
  }

  handleCheckboxChange(event) {
        this.setState({
            checkboxValue: event.target.checked
        });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="App-intro">
            <div className="input-container">
                <div className="input-title">Input</div>

                <Input type="text"
                       name="Text"
                       value={this.state.inputMessage}
                       handleChange={this.handleInputChange}/>


                <Input type="checkbox"
                       name="Checkpoint"
                       value={this.state.checkboxValue}
                       handleChange={this.handleCheckboxChange}/>
            </div>

            <div className="output-container">
                <div className="output-title">Output</div>

                <Output type="text" outputMessage={this.state.inputMessage} />
                <Output type="checkbox" outputMessage={this.state.checkboxValue} />
            </div>
        </div>
      </div>
    );
  }
}

export default App;
