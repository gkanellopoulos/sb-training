import React, { Component } from 'react';

class Output extends Component {
  render() {
    return (
      <div className="row">
            {this.props.type === "text" ?
                this.props.outputMessage :
                <input type="checkbox" checked={this.props.outputMessage} />
            }
      </div>
    );
  }
}

export default Output;
