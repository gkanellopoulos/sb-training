import React, { Component } from 'react';

class Input extends Component {
  render() {
    return (
      <div className="row">
          <div className="row-name">{this.props.name}:</div>
          <div className="row-value">
              <input type={this.props.type}
                     value={this.props.value}
                     onChange={this.props.handleChange} />
         </div>     
      </div>
    );
  }
}

export default Input;
